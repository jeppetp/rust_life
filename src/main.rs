use std::{thread, time};

const GRID_SIZE: (usize, usize) = (10, 10);

const FRAME_TIME: time::Duration = time::Duration::from_millis(100);

type Grid = [[bool; GRID_SIZE.0]; GRID_SIZE.1];

const TEST_GRID: Grid = [
    [
        true, false, false, false, true, false, false, false, false, false,
    ],
    [
        false, true, true, false, false, false, false, false, false, false,
    ],
    [
        true, true, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
    [
        false, false, false, false, false, false, false, false, false, false,
    ],
];

fn print_grid(grid: Grid) {
    for row in grid {
        for x in row {
            let grid_char: char = if x { '✨' } else { '🟥' };
            print!("{grid_char}")
        }
        println!("")
    }
}

fn neighbors(coord: (usize, usize), grid_size: (usize, usize)) -> Vec<(usize, usize)> {
    let mut list: Vec<(usize, usize)> = [].to_vec();
    for x in [1, 0, -1] as [i32; 3] {
        for y in [1, 0, -1] {
            if 0 <= x + coord.0 as i32
                && x + (coord.0 as i32) < grid_size.0 as i32
                && 0 <= y + coord.1 as i32
                && y + (coord.1 as i32) < grid_size.1 as i32
                && (x, y) != (0, 0)
            {
                list.push(((x + coord.0 as i32) as usize, (y + coord.1 as i32) as usize))
            }
        }
    }
    return list;
}

fn count_active_neighbors(coord: (usize, usize), grid: Grid) -> usize {
    let neighbors = neighbors(coord, (GRID_SIZE.1, GRID_SIZE.0));
    return neighbors
        .iter()
        .map(|x| grid[x.1][x.0])
        .filter(|x| *x)
        .collect::<Vec<bool>>()
        .len();
}

fn life(pixel_active: bool, num_neighbors_active: usize) -> bool {
    return num_neighbors_active == 3 || (num_neighbors_active == 2 && pixel_active);
}

fn update_grid(grid: Grid) -> Grid {
    let mut return_grid: Grid = [[false; GRID_SIZE.1]; GRID_SIZE.0];

    for y in 0..GRID_SIZE.0 {
        for x in 0..GRID_SIZE.1 {
            return_grid[y][x] = life(grid[y][x], count_active_neighbors((x, y), grid))
        }
    }
    return return_grid;
}

fn main() {
    let mut grid: Grid = TEST_GRID;
    loop {
        print_grid(grid);
        let separator: String = "--".repeat(GRID_SIZE.0);
        println!("{}", separator);
        grid = update_grid(grid);
        thread::sleep(FRAME_TIME)
    }
}
